from sklearn.feature_extraction.text import CountVectorizer

class CountFeatureExtractor(object):
    def fit_transform(self, corpus, *args, **kwargs):
        self.vectorizer = CountVectorizer()
        return self.vectorizer.fit_transform([' '.join(x) for x in corpus])

    def transform(self, corpus, *args, **kwargs):
        return self.vectorizer.transform([' '.join(x) for x in corpus])
