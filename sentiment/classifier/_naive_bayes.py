from sklearn.naive_bayes import BernoulliNB
import logging as log

class NaiveBayesClassifier(object):
    def __init__(self, feature_extractor):
        self.feature_extractor = feature_extractor

    def train(self, corpus, sentiments, *args, **kwargs):
        self.learner = BernoulliNB()
        log.info('Feature Extraction for Fit Begin')
        features = self.feature_extractor.fit_transform(corpus)
        log.info('Feature Extraction for Fit End')

        log.info('Classifier Fit Begin')
        self.learner.fit(features, sentiments) 
        log.info('Classifier Fit End')

    def test(self, corpus, sentiments, *args, **kwargs):
        if self.feature_extractor is None:
            raise RuntimeError("Test called before training occured.")

        log.info('Feature Extraction Begin')
        features = self.feature_extractor.transform(corpus)
        log.info('Feature Extraction Complete')

        log.info('Test Begin')
        score = self.learner.score(features, sentiments)
        log.info('Score: %g', score)
        log.info('Test Complete')
