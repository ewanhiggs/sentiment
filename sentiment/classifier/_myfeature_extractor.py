import numpy as np

class MyFeatureExtractor(object):
    def fit_transform(self, corpus, *args, **kwargs):
        self.all_lemmas = set()
        for lemmas in corpus:
            self.all_lemmas = all_lemmas.union(set(lemmas))
        log.info('Found %d training samples, total lemmas: %d',
                len(corpus), len(self.all_lemmas))

        self.all_lemmas = sorted(all_lemmas)
        return self.transform(corpus, *args, **kwargs)

    def transform(self, corpus, *args, **kwargs):
        all_features = []
        dtype = np.bool_
        for lemmas in lemmas_by_sample:
            lemmas_set = set(lemmas)
            feature_list = [lemma in lemmas_set for lemma in all_lemmas]
            features = np.array(feature_list, dtype=np.bool_)
            all_features.append(features)
        all_features = np.vstack(all_features)
        return all_features       
