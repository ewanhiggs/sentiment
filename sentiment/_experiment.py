import logging as log
import random
import nltk
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import BernoulliNB
from sklearn.metrics import classification_report
import numpy as np

class SingleExperiment(object):
    '''
    A simple entry point to run a classifier experiment using a yaml loader.
    '''
    def __init__(self, loader, classifier, reporter):
        self.loader = loader
        self.classifier = classifier
        self.report = reporter

    def __call__(self, *args, **kwargs):
        '''Load training data, train classifier, run against test data.'''
        log.info('Loading Begin')
        docs = [(doc['text'], doc['sentiment']) for doc in self.loader()]

        random.seed(1)
        random.shuffle(docs)
        log.info('Loading Complete')

        # 25% is training data; the rest is test.
        training_data = docs[:int(len(docs)/4)]
        test_data = docs[int(len(docs)/4):]

        log.info('Training data: %d entries', len(training_data))
        log.info('Test data: %d entries', len(test_data))

        training_corpus, training_sentiments = zip(*training_data)
        training_sentiments = np.array(training_sentiments)

        self.classifier.train(training_corpus, training_sentiments)

        test_corpus, test_sentiments = zip(*test_data)
        test_sentiments = np.array(test_sentiments)
        self.classifier.test(test_corpus, test_sentiments)

        self.report(self.classifier)
