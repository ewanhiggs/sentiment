from nltk.tokenize import TreebankWordTokenizer
import logging as log

class Tokenizer(object):
    '''
    A simple component that will take a string and return a list of strings
    which are tokens.
    '''
    def __call__(self, text, *args, **kwargs): 
        tokenizer = TreebankWordTokenizer()
        tokens = tokenizer.tokenize(text)
        return tokens
