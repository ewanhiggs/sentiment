from nltk.corpus import wordnet
import nltk
import logging as log


class NLTKLemmatizer(object):
    '''
    A component that accepts a list of strings and returns a list of strings in
    their lemmatized form. A lemma is the version of a word that you might find
    in a dictionary.

    If the word is not successfully lemmatized then the token is used verbatim.

    NLTK is known to be slow due to its Part of Speech tagger.
    '''
    def _lemmatize(self, token, tag):
        lemma = wordnet.morphy(token, _translated_tag(tag))
        return lemma if lemma else token

    def __call__(self, tokens, *args, **kwargs):
        log.info('Part of Speech Tagging Begin')
        tagged_tokens = nltk.pos_tag(tokens)
        log.info('Part of Speech Tagging Complete')
        log.info('Lemmatize Begin')
        lemmatized = [self._lemmatize(token, tag) for token, tag in tagged_tokens]
        log.info('Lemmatize End')
        return lemmatized

def _translated_tag(tag):
    tags = {
            'NN': wordnet.NOUN,
            'VB': wordnet.VERB,
            'RB': wordnet.ADV,
            'JJ': wordnet.ADJ,
            }
    return tags.get(tag, None)
