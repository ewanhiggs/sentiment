import logging as log

class Preprocessor(object):
    def __init__(self, tokenizer, lemmatizer, negator):
        self.tokenizer = tokenizer
        self.lemmatizer = lemmatizer
        self.negator = negator

    def __call__(self, text, *args, **kwargs):
        log.info('Tokenize Begin')
        tokens = self.tokenizer(text)
        log.info('Tokenize Complete')

        log.info('Lemmatize Begin')
        lemmas = self.lemmatizer(tokens)
        log.info('Lemmatize Complete')

        log.info('Negation Begin')
        negated_lemmas = self.negator(lemmas)
        log.info('Negation Complete')

        return negated_lemmas


