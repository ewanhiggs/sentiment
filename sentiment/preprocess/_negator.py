class Negator(object):
    '''
    Component that will replace tokens between negating token and next
    punctuation with 
    '''

    def __call__(self, tokens, *args, **kwargs):
        negating = False
        for i, token in enumerate(tokens):
            if token in ['not', "n't", 'never', 'no']:
                negating = True
            elif token in [',', '.', '(',')', 'but']:
                negating = False
            elif negating is True:
                tokens[i] = 'NOT_%s' % tokens[i]
        return tokens

class Noop(object):
    '''Component that does not attempt to negate anything'''
    def __call__(self, tokens, *args, **kwargs):
        return tokens
