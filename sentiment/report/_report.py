class Report(object):
    '''
    This is a placeholder component where we can report the performance of the
    classifier. For example, report the accuracy, plot correlations with other
    classifiers, and report on which features were most effective in predicting
    results.
    '''
    def __call__(self, *args, **kwargs):
        pass
