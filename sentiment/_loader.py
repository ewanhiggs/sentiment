import logging as log
import os
from os.path import join
import json

class RawLoader(object):
    '''
    A component that takes two directories of documents and loads them,
    returning a tuple of string and integer [-1, 1]
    '''
    def __init__(self, positive, negative, preprocessor):
        self.positive = positive
        self.negative = negative
        self.preprocessor = preprocessor

    def __call__(self):
        for root, dirs, files in os.walk(self.positive):
            for name in files:
                filepath = join(root, name)
                doc = open(filepath, 'r').read()
                processed_doc = self.preprocessor(doc)
                yield dict(text=processed_doc,
                        sentiment=1,
                        filename=name)

        for root, dirs, files in os.walk(self.negative):
            for name in files:
                filepath = join(root, name)
                doc = open(filepath, 'r').read()
                processed_doc = self.preprocessor(doc)
                yield dict(text=processed_doc,
                        sentiment=-1,
                        filename=name)

class JsonLoader(object):
    '''
    Component that loads a json file which has already been preprocessed.
    This should be a json object with at least 2 fields: "text" and "sentiment".
    '''
    def __init__(self, positive, negative):
        self.positive = positive
        self.negative = negative

    def __call__(self):
        for root, dirs, files in os.walk(self.positive):
            for name in files:
                filepath = join(root, name)
                doc = json.load(open(filepath, 'r'))
                yield dict(text=doc['text'],
                        sentiment=int(doc['sentiment']))

        for root, dirs, files in os.walk(self.negative):
            for name in files:
                filepath = join(root, name)
                doc = json.load(open(filepath, 'r'))
                yield dict(text=doc['text'],
                        sentiment=int(doc['sentiment']))
