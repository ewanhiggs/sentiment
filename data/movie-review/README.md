# txt\_sentoken
Raw text samples from Pang et al's Move Review data.

# sample

A subset of the text samples from txt\_sentoken

# json

Tokenized, lemmatized, and negated text in json format w/ the sentiment
information.
