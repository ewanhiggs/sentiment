# Introduction
This is a simple sentiment analysis toolkit. It uses yaml to drive experiments.

# Ideas/Todo
* Make a version using MaxEnt classifier. Make a version mixing the classifiers.
* Use feature selection to prune the classifiers to only those that are
  relevant. (cpu and memory consumption speedup).
* Expand on reporting to show all the relevant statistical scores. e.g. to show
  that MaxEnt is better/worse than NaiveBayes.
* Expand on experiments to perform cross validation.

# Requirements for Analysis
* libyaml (`brew install libyaml`)
* pyyaml (`pip install pyyaml`)
* nltk (`pip install nltk`)
  * `nltk.download()` -> `maxent_treebank_pos_tagger`
  * `nltk.download()` -> `treebank`
  * `nltk.download()` -> `wordnet`

# Resources
[Movie review data](http://www.cs.cornell.edu/people/pabo/movie-review-data/)
