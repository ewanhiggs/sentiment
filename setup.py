#!/usr/bin/env python

from distutils.core import setup

setup(name='Sentiment',
      version='0.1',
      description='Sentiment Analysis Workbench',
      author='Ewan Higgs',
      author_email='ewan_higgs@yahoo.co.uk',
      url='http://bitbucket.org/ewanhiggs/sentiment',
      packages=['sentiment', 'sentiment.preprocess', 'sentiment.classifier',
          'sentiment.report'],
      scripts = ['bin/runner', 'bin/lemmatize'],
      install_requires=['yaml', 'nltk', 'numpy', 'scikit-learn'],
     )
